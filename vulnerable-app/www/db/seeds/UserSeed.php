<?php


use Phinx\Seed\AbstractSeed;

class UserSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'prenom'    => 'Homer',
                'nom' => 'Simpson',
                'password' =>  'd048e0f7f525f28595f4297219de507e', // donuts1
                'image' => 'img/pic.jpg',
                'bio' => "C'est pas parce que je m\'en fous que je comprends pas.",
            ],
            [
                'prenom'    => 'Marge',
                'nom' => 'Simpson',
                'password' =>  '70b959c40fe68e799d54296ba32834ef',
                'image' => 'img/pic2.jpg',
                'bio' => "Quand une femme dit que tout va bien, ça veut dire que tout va mal",
            ],
            [
                'prenom'    => 'Montgomery',
                'nom' => 'Burns',
                'password' =>  '9726255eec083aa56dc0449a21b33190',
                'image' => 'img/pic3.jpg',
                'bio' => "Famille, religion, amitié. Voici les 3 démons que vous devez abattre pour réussir dans les affaires.",
            ],
            [
                'prenom'    => 'Moe',
                'nom' => 'Szyslak',
                'password' =>  '3e1867f5aee83045775fbe355e6a3ce1',
                'image' => 'img/pic4.jpg',
                'bio' => "On pourrait vendre de l\'alcool. Je fais ça très bien.",
            ],
        ];

        $posts = $this->table('users');
        $posts->insert($data)
              ->saveData();
    }
}
