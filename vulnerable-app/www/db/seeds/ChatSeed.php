<?php


use Phinx\Seed\AbstractSeed;

class ChatSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'message' => 'Salut, on test le chat ?',
                'de' =>  'Homer',
            ],
            [
                'message' => 'Ca marche, allé viens manger !',
                'de' =>  'Marge',
            ],[
                'message' => 'Tu passes au bar ?',
                'de' =>  'Moe',
            ],[
                'message' => 'Je ne pense vraiment pas que ca soit une bonne idée Moe... Homer doit aussi se soucier de sa famille, il n\'a toujours pas diné avec nous alors que c\'est le premier assis a table habituellement .... Je ne le comprend plus depuis qu\'il est devenu Administrateur de ce site \"Noob\" ... il ne fait que bosser .. Je ne reconnait plus mon homer :(',
                'de' =>  'Marge',
            ],[
                'message' => 'Un brave type cet Homer ...',
                'de' =>  'Montgomery',
            ],
            
        ];

        $posts = $this->table('chat');
        $posts->insert($data)
              ->saveData();
    }
}
