<?php
session_start();
try
{
    require_once(__DIR__.'/../lib/connection.php');
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}
$req = $bdd->prepare('INSERT INTO chat(message,de) VALUES (:message,:de)');
$req->execute(array(
	'message' => $_POST['message'],
	'de' => $_SESSION['username']
));