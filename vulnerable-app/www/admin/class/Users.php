<?php

require_once(__DIR__.'/connection.php');

final class Users {

    function getAll(){
        $bdd = new Connection();
        $bdd = $bdd->get();
        $req = $bdd->prepare('SELECT * FROM users');
        $req->execute();
        $users = $req->fetchAll();
        return $users;
    }

    function getById($id){
        $bdd = new Connection();
        $bdd = $bdd->get();
        $req = $bdd->prepare('SELECT * FROM users WHERE id='.$id);
        $req->execute();
        $user = $req->fetch();
        return $user;
    }

    function getByprenom($username){
        $bdd = new Connection();
        $bdd = $bdd->get();
        $req = $bdd->prepare('SELECT * FROM users WHERE prenom="'.$username.'"');
        $req->execute();
        $user = $req->fetch();
        return $user;
    }

}