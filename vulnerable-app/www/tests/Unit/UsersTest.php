<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../../admin/class/Users.php');

final class UsersTest extends TestCase
{
    public function testGetAllReturnsArray()
    {
        $users = new Users();
        $result = $users->getAll();
        $this->assertIsArray($result);
    }

    public function testGetByIdReturnsUser()
    {
        $users = new Users();
        $result = $users->getById(1);
        $this->assertEquals('Homer', $result['prenom']);
        $this->assertEquals('Simpson', $result['nom']);
    }
}